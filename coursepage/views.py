from django.shortcuts import render

def coursepage(request):
    return render(request, "coursepage/coursepage.html")
